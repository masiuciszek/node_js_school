const express = require('express');
const path = require('path');

const app = express();
console.log(path.join(__dirname, '../public'));

const publicDirectory = path.join(__dirname, '../public');
const partialsPath = path.join(__dirname, '../templates/partials');
const viewsPath = path.join(__dirname, '../templates/views');

app.use(express.static(publicDirectory));

app.set('view engine', 'pug');
app.set('views', viewsPath);
app.set('partials', partialsPath);

app.get('/', (req, res) => {
  res.render('index', {
    topTitle: 'Home',
    title: 'Welcome master',
  });
});
app.get('/about', (req, res) => {
  res.render('about', {
    topTitle: 'About',
    title: 'Welcome to the about page',
  });
});
app.get('/contact', (req, res) => {
  res.render('contact', {
    topTitle: 'Contact',
    title: 'Contact page',
  });
});

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server is running boooi ${PORT}`));
