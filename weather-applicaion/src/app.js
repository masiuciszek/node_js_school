const express = require('express');
const path = require('path');
const app = express();
const geocode = require('./utils/geocode');
const forecast = require('./utils/forecast');
const randomWords = require('./utils/helper');

const publicPath = path.join(__dirname, '../public');
const parialsPath = path.join(__dirname, '../templates/partials');
const viewsPath = path.join(__dirname, '../templates/views');

app.set('view engine', 'pug');
app.set('views', viewsPath);
app.set('partials', parialsPath);

app.use(express.static(publicPath));

const PORT = process.env.PORT || 3000;

app.get('/', (req, res) => {
  res.render('index', {
    title: 'Weather Page',
    titleBody: 'Welcome',
    name: 'Marcell'
  });
});

app.get('/weather', (req, res) => {
  if (!req.query.address) {
    return res.send({
      error: 'please provide an address'
    });
  }
  const { address } = req.query;
  geocode(address, (error, { latitude, longitude, location } = {}) => {
    if (error) {
      return res.send({ error });
    }
    forecast(latitude, longitude, (error, forecastData) => {
      if (error) {
        return res.send({ error });
      }

      res.render('weather', {
        forecast: forecastData,
        location,
        address: `${address} ${randomWords()}`
      });
    });
  });
});

app.get('*', (req, res) => {
  res.render('404', {
    title: '404 page',
    titleBody: 'Oopp we cot on 404',
    name: 'Marcell'
  });
});

app.listen(PORT, () => console.log(`Port is running on ${PORT}`));
