const request = require('request');
require('dotenv').config();

const geocode = (address, fn) => {
  const url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${address}.json?access_token=${
    process.env.MAPBOX_KEY
  }&limit=1`;

  request({ url, json: true }, (error, { body }) => {
    if (error) {
      fn('Unable to connect to location Services!!!', undefined);
    } else if (body.features.length === 0) {
      fn('Unable to find location, Try another Service!!!', undefined);
    } else {
      const { center, place_name } = body.features[0];
      fn(undefined, {
        longitude: center[1],
        latitude: center[0],
        location: place_name
      });
    }
  });
};

module.exports = geocode;
