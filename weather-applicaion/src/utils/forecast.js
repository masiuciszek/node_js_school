const request = require('request');
require('dotenv').config();

const forecast = (longitude, latitude, fn) => {
  const url = `https://api.darksky.net/forecast/${
    process.env.DARK_SKY_KEY
  }/${longitude},${latitude}?units=si`;

  request({ url, json: true }, (error, { body }) => {
    if (error) {
      fn('Unable to get the data', undefined);
    } else if (body.error) {
      fn('Unable to get the right cordinates', undefined);
    } else {
      const { summary } = body.daily.data[0];
      const { temperature, precipProbability } = body.currently;
      fn(
        undefined,
        `${summary} it is currently ${temperature} degrees outside, there is a ${precipProbability}% chamce of rain`
      );
    }
  });
};

module.exports = forecast;
