const strangeWords = [
  'been there',
  'i Liket it ',
  'aawww I love it over there ',
  'I want to go there'
];

const randomWords = () => {
  const mixed = strangeWords[Math.floor(Math.random() * strangeWords.length)];

  return mixed;
};

module.exports = randomWords;
