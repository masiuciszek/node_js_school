const form = document.querySelector('form');
const input = document.querySelector('input');

form.addEventListener('submit', e => {
  e.preventDefault();
  const location = input.value;
  fetch(`http://localhost:3000/weather?address=${location}`).then(response => {
    response.json().then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        console.log(data.location);
      }
    });
  });
  input.value = '';
});
