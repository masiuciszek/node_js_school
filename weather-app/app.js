const request = require('request');
const axios = require('axios');
require('dotenv').config();

const url = process.env.WEATHER_API_KEY;
const geoCodeUrl = process.env.GEOCODE_URL;

// const res = axios
//   .get(
//     url
//   )
//   .then(r => console.log(r.data.currently));

// request({ url, json: true }, (err, res) => {
//   if (err) console.error('Pronlem to connect to the service!');
//   const { summary, pressure, ozone } = res.body.currently;
//   const {
//     summary: summary2,
//     data: { time }
//   } = res.body.hourly;
//   console.log(
//     `today it is ${summary} and pressure is ${pressure}  the ozone is ${ozone} ${time}`
//   );
// });

// const res = axios
//   .get(geoCodeUrl)
//   .then(res => {
//     const data = res.data.features;
//     const test = data.map(x => x.geometry.coordinates);
//     const [long, ati, ...rest] = test;
//     const [l, la] = long;
//     const [l2, la2] = ati;

//     console.log(`The latitude is ${l} and the longitude is ${la} `);
//   })
//   .catch(err => {
//     console.log('Sorry there was a problem!!! ', err.message);
//   });

// console.log(res);

const geoCode = (address, cb) => {
  const url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${encodeURIComponent(
    address
  )}.json?access_token=pk.eyJ1IjoibWFzaXVjaXN6ZWsiLCJhIjoiY2p4b2VxYmQ2MDVuaTNkbW9kc3B4NXl1NyJ9.NItVqJdiRodEQRcvZ7LQkg`;

  request({ url, json: true }, (err = undefined, res = undefined) => {
    if (err) {
      cb('Unable to connect!!');
    } else if (res.body.features.length === 0) {
      cb('Unable to connect!!');
    } else {
      const [long, lat] = res.body.features;
      const { bbox } = long;
      const { bbox: bbox2 } = lat;
      const [lo, la] = bbox;
      const [lo2, la2] = bbox2;
      cb({ lo, la });
    }
  });
};

geoCode('Texas', (err, data) => {
  console.log('Error', err);
  console.log('Data', data);
});
