exports.catchErrors = fn => (...args) =>
  fn(...args).catch(e => console.log(e.message));
