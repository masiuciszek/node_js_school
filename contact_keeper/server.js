const express = require('express');
const connectDB = require('./config/db');
const app = express();
const { catchErrors } = require('./helper');

// connect to DB
catchErrors(connectDB());

// Init Middleware
app.use(express.json({ extended: false }));

catchErrors(app.use('/api/users', require('./routes/users')));
catchErrors(app.use('/api/auth', require('./routes/auth')));
catchErrors(app.use('/api/contacts', require('./routes/contacts')));

const port = process.env.PORT || 4000;

app.listen(port, () => console.log(`Server is running on Port ${port}`));
