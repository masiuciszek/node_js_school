const mongoose = require('mongoose');

const { Schema } = mongoose;

const ContactSchema = Schema({
  // /relation ship with the user schema
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  phone: {
    type: String
  },
  city: {
    type: String
  },
  type: {
    type: String,
    default: 'personal'
  },
  phone: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('contact', ContactSchema);
