const square = x => x * x;
// console.log(square(10, 10));

const tasks = {
  tasks: [
    {
      text: 'shooping',
      done: false
    },
    {
      text: 'Running',
      done: true
    },
    {
      text: 'Sleepin',
      done: false
    },
    {
      text: 'walking with the dog',
      done: false
    }
  ],
  getDonteTasks() {
    return this.tasks.filter(t => t.done);
  }
};

console.log(tasks.getDonteTasks());
