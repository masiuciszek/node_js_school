const add = (a, b, fn) => {
  setTimeout(() => {
    fn(a + b);
  }, 2000);
};

add(6, 7, sum => {
  console.log(sum);
});
