const url =
  'https://api.mapbox.com/geocoding/v5/mapbox.places/Los%20Angeles.json?access_token=pk.eyJ1IjoibWFzaXVjaXN6ZWsiLCJhIjoiY2p4b2VxYmQ2MDVuaTNkbW9kc3B4NXl1NyJ9.NItVqJdiRodEQRcvZ7LQkg';

const dataArea = document.querySelector('.data');

const geoloacation = data => {
  fetch(data)
    .then(res => res.json())
    .then(r => {
      console.log(r.features);
      for (let val of r.features) {
        let output = `
            <div>
                <h1>
                    ${val.type}
                </h1>
            </div>
        
        `;
        dataArea.innerHTML = output;
      }
    })
    .catch(err => console.error(err));
};

geoloacation(url);
