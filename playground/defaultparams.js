const greeter = (name = 'marcello', age = 22) => {
  return 'hello ' + name + ' is your current age ' + age + '?';
};

console.log(greeter());
