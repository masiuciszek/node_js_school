const Mongodb = require('mongodb');
require('dotenv').config();

const { MongoClient, ObjectID } = Mongodb;

const dataBase = "dang-thats-delicious's";
const connectionUrl = `mongodb://${process.env.CONNECTION_URL}`;

MongoClient.connect(connectionUrl, { useNewUrlParser: true }, (err, client) => {
  if (err) {
    return console.log('Unable to connect to Database!!!');
  }
  // console.log('Connected to MongoDB!');
  const db = client.db(dataBase);

  db.collection('tasks').insertMany(
    [
      {
        description: 'Clean the house',
        completed: true,
      },
      {
        description: 'Renew inspection',
        completed: false,
      },
    ],
    (error, result) => {
      if (error) {
        return console.log('Unable to insert tasks!');
      }
      console.log(result.ops);
    }
  );
});
