const fs = require('fs');
const chalk = require('chalk');

const laodNotes = () => {
  try {
    const notes = fs.readFileSync('notes.json');
    const data = notes.toString();
    return JSON.parse(data);
  } catch (err) {
    console.error(err.message);
    return [];
  }
};

const saveNote = note => {
  const notes = JSON.stringify(note);
  fs.writeFileSync('notes.json', notes);
};

const addNote = (title, body) => {
  const notes = laodNotes();
  const uniqueNotes = notes.find(n => n.title === title);
  if (!uniqueNotes) {
    notes.push({
      title,
      body
    });
    console.log(chalk.bgGreen.black('Note Added!!!'));
    saveNote(notes);
  } else {
    console.log(chalk.bgRed.black('sorry not already exits'));
  }
};

const deleteNote = title => {
  const notes = laodNotes();
  const noteToKeep = notes.filter(n => n.title !== title);
  if (notes.length > noteToKeep.length) {
    console.log(chalk.bgBlue('Note Deleted!!!'));
    saveNote(noteToKeep);
  } else {
    console.log(chalk.bgRed.black('No Note found'));
  }
};

const listNotes = () => {
  const notes = laodNotes();
  return notes.forEach(note => {
    const { title, body } = note;
    console.log(chalk.bgBlue(title, body));
  });
};

const readNote = title => {
  const notes = laodNotes();
  const note = notes.find(n => n.title === title);
  if (note) {
    const { title, body } = note;
    console.log(chalk.bgGreen.black(title, ' || ', body));
    console.log(note.body);
    if (title === 'ifk') console.log(chalk.bgBlue(title));
  } else {
    console.log(chalk.bgGreen.black("note does's not exit in the database"));
  }
};

module.exports = {
  addNote,
  deleteNote,
  listNotes,
  readNote
};
