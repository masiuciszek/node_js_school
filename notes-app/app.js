const yargs = require('yargs');
const notes = require('./notes');

yargs.version('1.1.0');

yargs.command({
  command: 'add',
  desc: 'adding a note',
  builder: {
    title: {
      desc: 'Notes title',
      demandOption: true,
      type: 'string'
    },
    body: {
      desc: 'Notes body',
      demandOption: true,
      type: 'string'
    }
  },
  handler(argv) {
    notes.addNote(argv.title, argv.body);
  }
});

yargs.command({
  command: 'remove',
  desc: 'remove a note',
  builder: {
    title: {
      desc: 'Note title',
      demandOption: true,
      type: 'string'
    }
  },
  handler(argv) {
    notes.deleteNote(argv.title);
  }
});

yargs.command({
  command: 'list',
  desc: 'listing notes',

  handler(argv) {
    notes.listNotes(argv.notes);
  }
});

yargs.command({
  command: 'read',
  desc: 'read your note',
  builder: {
    title: {
      desc: 'read the note',
      demandOption: true,
      type: 'string'
    }
  },
  handler(argv) {
    notes.readNote(argv.title, argv.body);
  }
});

yargs.parse();
